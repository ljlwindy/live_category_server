package main

import (
	"live_category_server/handler"

	"github.com/micro/go-micro"

	"fmt"
	"proto/category"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("go.micro.srv.category"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init()

	// Register Handler
	category.RegisterCategoryHandler(service.Server(), new(handler.CategoryHandler))

	// Run service
	if err := service.Run(); err != nil {
		fmt.Println(err)
	}
}
