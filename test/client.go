package main

import (
	"context"
	"fmt"

	proto "proto/category"

	micro "github.com/micro/go-micro"
)

func main() {
	// Create a new service. Optionally include some options here.
	service := micro.NewService(micro.Name("category.client"))
	service.Init()

	// Create new category client
	client := proto.NewCategoryService("go.micro.srv.category", service.Client())

	// Call the category
	rsp, err := client.Getfrontpage(context.TODO(), &proto.GetFrontPageRequest{Uid: 123})
	if err != nil {
		fmt.Println(err)
	}

	// Print response
	fmt.Println(rsp.Context)
	fmt.Println(rsp.Result.Result)
	fmt.Println(rsp.Result.Desc)
}
