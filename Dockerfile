FROM alpine
ADD live_category_server-srv /live_category_server-srv
ENTRYPOINT [ "/live_category_server-srv" ]
